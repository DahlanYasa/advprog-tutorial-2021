####Lazy Instantiation
Lazy Instantiation adalah instansiasi objek satu kali ketika Class digunakan .
Jadi di sini kita menginisialisasi instance Singleton dengan cara lazy di mana kita membuatnya
diinisialisasi ketika seseorang membutuhkannya, jika tidak maka tidak akan pernah diinisialisasi.

Kelebihan : lebih hemat memori dibanding Eager Instantiation, karena instance hanya dibuat jika class tersebut digunakan saat jalannya program.
Kekurangan : Lazy Instantiation dapat menjadi performance issue ketika menggunakan multithreaded environment,
lalu lazy instantiation menggunakan JVM karena dapat terjadi pemanggilan getInstance() pada waktu yang bersamaan.
dan Selalu melakukan pengecekan setiap getInstation.

####Eager Instantiation
Object dibuat saat class tersebut di load. Tidak tergantung apakah class tersebut akan dipakai atau tidak.

Kelebihan : thread-safe.
kekurangan : Instansiasi tetap akan dibuat walaupun tidak digunakan, dan overhead yang
semakin banyak apabila penggunaan singleton banyak.