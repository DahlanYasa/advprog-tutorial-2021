package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

public class CheeseTest {
    private Class<?> cheeseClass;

    @BeforeEach
    public void setup() throws Exception {
        cheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese");
    }

    @Test
    public void testCheeseIsConcreteClass() {
        int classModifiers = cheeseClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testCheeseIsATopping() {
        Collection<Type> interfaces = Arrays.asList(cheeseClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping")));
    }

    @Test
    public void testCheeseOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = cheeseClass.getDeclaredMethod("getDescription");
        int methodModifiers = getDescription.getModifiers();

        assertEquals("java.lang.String", getDescription.getGenericReturnType().getTypeName());
        assertEquals(0, getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCheeseGetDescriptionShouldReturnDescription() throws Exception {
        Cheese cheese = new Cheese();
        assertEquals("Adding Shredded Cheese Topping...", cheese.getDescription());
    }
}
