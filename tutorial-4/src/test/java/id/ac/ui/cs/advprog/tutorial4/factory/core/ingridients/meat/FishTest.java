package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

public class FishTest {
    private Class<?> fishClass;

    @BeforeEach
    public void setup() throws Exception {
        fishClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish");
    }

    @Test
    public void testFishIsConcreteClass() {
        int classModifiers = fishClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testFishIsAMeat() {
        Collection<Type> interfaces = Arrays.asList(fishClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat")));
    }

    @Test
    public void testFishOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = fishClass.getDeclaredMethod("getDescription");
        int methodModifiers = getDescription.getModifiers();

        assertEquals("java.lang.String", getDescription.getGenericReturnType().getTypeName());
        assertEquals(0, getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFishGetDescriptionShouldReturnDescription() throws Exception {
        Fish fish = new Fish();
        assertEquals("Adding Zhangyun Salmon Fish Meat...", fish.getDescription());
    }
}
