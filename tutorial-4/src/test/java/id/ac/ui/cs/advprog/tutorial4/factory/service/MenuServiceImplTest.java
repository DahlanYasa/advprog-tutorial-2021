package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class MenuServiceImplTest {

    private MenuServiceImpl menuService;

    @Mock
    MenuRepository repo;

    @BeforeEach
    public void setUp() throws Exception {
        menuService = new MenuServiceImpl();
    }

    @Test
    public void testMenuServiceImplHasGetMenusImplementation() {
        assertEquals(4, menuService.getMenus().size());
    }

    @Test
    public void testMenuServiceImplHasCreateMenuImplementation() {
        menuService.createMenu("Dummy Soba", "Soba");
        menuService.createMenu("Dummy Udon", "Udon");
        menuService.createMenu("Dummy Ramen", "Ramen");
        menuService.createMenu("Dummy Shirataki", "Shirataki");

        assertEquals(8, menuService.getMenus().size());
    }
}
