package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngridientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MenuTest {
    private Class<?> menuClass;

    private Menu menu;

    @Mock
    private IngridientsFactory factory;

    @BeforeEach
    public void setUp() throws Exception {
        menuClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
        factory = mock(IngridientsFactory.class);
        menu = new Menu("Dummy", factory);
    }

    @Test
    public void testMenuIsAPublicClass() {
        int classModifiers = menuClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testMenuHasGetNameMethod() throws Exception {
        Method getName = menuClass.getDeclaredMethod("getName");
        int methodModifiers = getName.getModifiers();

        assertEquals("java.lang.String", getName.getGenericReturnType().getTypeName());
        assertEquals(0, getName.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMenuHasGetNoodleMethod() throws Exception {
        Method getNoodle = menuClass.getDeclaredMethod("getNoodle");
        int methodModifiers = getNoodle.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                getNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, getNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMenuHasGetMeatMethod() throws Exception {
        Method getMeat = menuClass.getDeclaredMethod("getMeat");
        int methodModifiers = getMeat.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                getMeat.getGenericReturnType().getTypeName());
        assertEquals(0, getMeat.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMenuHasGetToppingMethod() throws Exception {
        Method getTopping = menuClass.getDeclaredMethod("getTopping");
        int methodModifiers = getTopping.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping", getTopping.getGenericReturnType().getTypeName());
        assertEquals(0, getTopping.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMenuHasGetFlavorMethod() throws Exception {
        Method getFlavor = menuClass.getDeclaredMethod("getFlavor");
        int methodModifiers = getFlavor.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor", getFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, getFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMenuHasPrepareMethod() throws Exception {
        Method prepare = menuClass.getDeclaredMethod("setIngridients");
        int methodModifiers = prepare.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(0, prepare.getParameterCount());
    }

    @Test
    public void testMenuPrepareMethodImplementedCorrectly() throws Exception {
        Noodle noodle = new Ramen();
        Flavor flavor = new Spicy();
        Meat meat = new Pork();
        Topping topping = new BoiledEgg();
        when(factory.createNoodle()).thenReturn(noodle);
        when(factory.createFlavor()).thenReturn(flavor);
        when(factory.createMeat()).thenReturn(meat);
        when(factory.createTopping()).thenReturn(topping);

        menu.setIngridients();

        assertThat(menu.getNoodle()).isInstanceOf(Ramen.class);
        assertThat(menu.getFlavor()).isInstanceOf(Spicy.class);
        assertThat(menu.getMeat()).isInstanceOf(Pork.class);
        assertThat(menu.getTopping()).isInstanceOf(BoiledEgg.class);
    }

    @Test
    public void testMenuGetNameMethodImplementedCorrectly() {
        assertEquals("Dummy", menu.getName());
    }
}
