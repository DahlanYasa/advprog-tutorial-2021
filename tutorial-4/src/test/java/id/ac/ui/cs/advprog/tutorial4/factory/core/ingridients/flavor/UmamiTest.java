package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

public class UmamiTest {
    private Class<?> umamiClass;

    @BeforeEach
    public void setup() throws Exception {
        umamiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami");
    }

    @Test
    public void testUmamiIsConcreteClass() {
        int classModifiers = umamiClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testUmamiIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(umamiClass.getInterfaces());
        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor")));
    }

    @Test
    public void testUmamiOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = umamiClass.getDeclaredMethod("getDescription");
        int methodModifiers = getDescription.getModifiers();
        assertEquals("java.lang.String", getDescription.getGenericReturnType().getTypeName());
        assertEquals(0, getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testUmamiGetDescriptionShouldReturnDescription() throws Exception {
        Umami umami = new Umami();
        assertEquals("Adding WanPlus Specialty MSG flavoring...", umami.getDescription());
    }
}
