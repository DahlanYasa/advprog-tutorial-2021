package id.ac.ui.cs.advprog.tutorial4.singleton.Service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {
    OrderFood orderFood;
    OrderDrink orderDrink;

    private Class<?> orderServiceClass;

    @InjectMocks
    private OrderServiceImpl orderService;

    @BeforeEach
    void setUp() throws ClassNotFoundException {
        orderServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.service" +
                ".OrderServiceImpl");
        orderFood = OrderFood.getInstance();
        orderDrink = OrderDrink.getInstance();
    }

    @Test
    public void testOrderServiceHasCorrectMethods() throws Exception {
        Method getDrink = orderServiceClass.getDeclaredMethod("getDrink");
        assertTrue(Modifier.isPublic(getDrink.getModifiers()));

        Method getFood = orderServiceClass.getDeclaredMethod("getFood");
        assertTrue(Modifier.isPublic(getFood.getModifiers()));
    }

    @Test
    void testOrderADrinkSuccess() {
        String drinkName = "New Drink";
        orderService.orderADrink(drinkName);
        assertEquals(drinkName, orderService.getDrink().getDrink());
    }

    @Test
    void testGetDrinkSuccess() {
        assertEquals(orderDrink, orderService.getDrink());
    }

    @Test
    void testOrderAFoodSuccess() {
        String foodName = "New Food";
        orderService.orderAFood(foodName);
        assertEquals(foodName, orderService.getFood().getFood());
    }

    @Test
    void testGetFoodSuccess() {
        assertEquals(orderFood, orderService.getFood());
    }

    @Test
    void testOrderFoodOnlyOneInstance() {
        OrderFood i1 = OrderFood.getInstance();
        OrderFood i2 = OrderFood.getInstance();
        assertEquals(System.identityHashCode(i1), System.identityHashCode(i2));
        assertEquals(System.identityHashCode(i1), System.identityHashCode(orderService.getFood()));
    }

    @Test
    void testOrderDrinkOnlyOneInstance() {
        OrderDrink i1 = OrderDrink.getInstance();
        OrderDrink i2 = OrderDrink.getInstance();
        assertEquals(System.identityHashCode(i1), System.identityHashCode(i2));
        assertEquals(System.identityHashCode(i1), System.identityHashCode(orderService.getDrink()));
    }
}
