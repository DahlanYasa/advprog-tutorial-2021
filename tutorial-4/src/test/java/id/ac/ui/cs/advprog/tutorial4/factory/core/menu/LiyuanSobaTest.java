package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class LiyuanSobaTest {
    private Class<?> liyuanSobaClass;

    private LiyuanSoba liyuanSoba;

    @BeforeEach
    public void setUp() throws Exception {
        liyuanSobaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba");
        liyuanSoba = new LiyuanSoba("DummyLiyuanSoba");
    }

    @Test
    public void testLiyuanSobaIsAPublicClass() {
        int classModifiers = liyuanSobaClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testLiyuanSobaIsAConcreteClass() {
        assertFalse(Modifier.isAbstract(liyuanSobaClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaGetNameShouldReturnName() {
        assertEquals("DummyLiyuanSoba", liyuanSoba.getName());
    }

    @Test
    public void testLiyuanSobaGetNoodleShouldReturnSoba() {
        Noodle noodle = liyuanSoba.getNoodle();
        assertThat(noodle).isInstanceOf(Soba.class);
    }

    @Test
    public void testLiyuanSobaGetMeatShouldReturnFish() {
        Meat meat = liyuanSoba.getMeat();
        assertThat(meat).isInstanceOf(Beef.class);
    }

    @Test
    public void testLiyuanSobaGetToppingShouldReturnMushroom() {
        Topping topping = liyuanSoba.getTopping();
        assertThat(topping).isInstanceOf(Mushroom.class);
    }

    @Test
    public void testLiyuanSobaGetFlavorShouldReturnSweet() {
        Flavor flavor = liyuanSoba.getFlavor();
        assertThat(flavor).isInstanceOf(Sweet.class);
    }
}
