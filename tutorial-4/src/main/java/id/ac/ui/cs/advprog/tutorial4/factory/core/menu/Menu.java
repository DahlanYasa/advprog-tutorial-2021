package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngridientsFactory;

public class Menu {
    private String name;
    private Noodle noodle;
    private Meat meat;
    private Topping topping;
    private Flavor flavor;
    private IngridientsFactory factory;

    //To Do : Complete Me
    //Silahkan tambahkan parameter jika dibutuhkan
    public Menu(String name, IngridientsFactory factory){
        this.name = name;
        this.factory = factory;
        setIngridients();
    }

    public String getName() {
        return name;
    }

    public Noodle getNoodle() {
        return noodle;
    }

    public Meat getMeat() {
        return meat;
    }

    public Topping getTopping() {
        return topping;
    }

    public Flavor getFlavor() {
        return flavor;
    }

    public void setIngridients() {
        this.noodle = factory.createNoodle();
        this.meat = factory.createMeat();
        this.topping = factory.createTopping();
        this.flavor = factory.createFlavor();
    }

}